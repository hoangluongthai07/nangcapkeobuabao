﻿﻿using System;

class Program
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.WriteLine("Trò chơi Kéo Búa Bao!");

        Console.WriteLine("Người chơi 1 đưa ra lựa chọn: ");
        Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");
        int player1Choice = int.Parse(Console.ReadLine());

        Console.WriteLine("Người chơi 2 đưa ra lựa chọn: ");
        Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");
        int player2Choice = int.Parse(Console.ReadLine());

        switch (player1Choice)
        {
            case 1:
                switch (player2Choice)
                {
                    case 1:
                        Console.WriteLine("Kết quả: Hòa!");
                        break;
                    case 2:
                        Console.WriteLine("Kết quả: Người chơi 2 chiến thắng!");
                        break;
                    case 3:
                        Console.WriteLine("Kết quả: Người chơi 1 chiến thắng!");
                        break;
                }
                break;
            case 2:
                switch (player2Choice)
                {
                    case 1:
                        Console.WriteLine("Kết quả: Người chơi 1 chiến thắng!");
                        break;
                    case 2:
                        Console.WriteLine("Kết quả: Hòa!");
                        break;
                    case 3:
                        Console.WriteLine("Kết quả: Người chơi 2 chiến thắng!");
                        break;
                }
                break;
            case 3:
                switch (player2Choice)
                {
                    case 1:
                        Console.WriteLine("Kết quả: Người chơi 2 chiến thắng!");
                        break;
                    case 2:
                        Console.WriteLine("Kết quả: Người chơi 1 chiến thắng!");
                        break;
                    case 3:
                        Console.WriteLine("Kết quả: Hòa!");
                        break;
                }
                break;
        }
    }
}